﻿using System;

namespace Assignment7
{
    class Program
    {
        private const int defaultConst = 999;
        static void Main(string[] args)
        {
            Invoice[] info = new Invoice[5];

            for (int i = 0; i < info.Length; i++)
            {
                try
                {
                    Console.Write("Enter invoice number >> ");
                    var idStr = Console.ReadLine();
                    Console.Write("Enter balance  >> ");
                    var balanceStr = Console.ReadLine();
                    Console.Write("Enter due day >> ");
                    var dueDayStr = Console.ReadLine();

                    var invoice = new Invoice(int.Parse(idStr), double.Parse(balanceStr), int.Parse(dueDayStr));
                    info[i] = invoice;
                }
                catch (Exception e)
                {
                    var invoice = new Invoice(defaultConst, 0, 1);
                    info[i] = invoice;
                }
            }
            Console.WriteLine("Entered invoices");
            for (int i = 0; i < info.Length; i++)
            {
                Console.WriteLine($"#{info[i].Id} {info[i].BalanceDue:c2} day {info[i].DayDue}");
            }

            Console.ReadLine();
        }
    }

    public class Invoice
    {
        private int id;
        private double balanceDue;
        private int dayDue;

        public Invoice(int id, double balanceDue, int dayDue)
        {
            if (id < 100 || id > 999)
            {
                throw new ArgumentException();
            }
            if (dayDue < 1 || dayDue > 31)
            {
                throw new ArgumentException();
            }

            this.id = id;
            this.balanceDue = balanceDue;
            this.dayDue = dayDue;
        }

        public int Id
        {
            get
            {
                return id;
            }

        }
        public double BalanceDue
        {
            get
            {
                return balanceDue;
            }

        }
        public int DayDue
        {
            get
            {
                return dayDue;
            }

        }
    }
}
